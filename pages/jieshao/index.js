// pages/jieshao/index.js
var app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    windowHeight: 0,
    headHeight: 0,
    bodyHeight: 0,
    top: 0,
    latitude: 0,//纬度 
    longitude: 0,//经度 
    speed: 0,//速度 
    accuracy: 16,//位置精准度 
    markers: [],
    covers: [],
    gs: 0,
    length: 0,
    style: '',
    status: 0,
    dpxinxi: null,
    url: '',
    titleimg: '/pages/images/title.jpg'
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(app.globalData.dpxinxi)
    var that = this
    wx.getSystemInfo({
      success: function (res) {
        var _number = "蔬力堡愿用美食来诉说爱，做一份至纯的食品，一份至善的事业！让人从身体到心灵都能变得健康轻松，用植物性饮食点亮世界。越蔬悦健康！"
        that.setData({
          windowHeight: res.windowHeight,
          headHeight: res.windowHeight * 0.4,
          bodyHeight: res.windowHeight * 0.68,
          top: res.windowHeight * 0.35,
          gs: parseInt((res.windowWidth * 0.9 * 0.94 / 16) * 4),
          length: _number.length,
          dpxinxi: app.globalData.dpxinxi,
          url: app.globalData.severUrl
        })
      }
    })

    if (this.data.length > this.data.gs) {
      this.setData({
        style: 'display:-webkit-box;overflow:hidden;text-overflow:ellipsis;-webkit-line-clamp:4;-webkit-box-orient:vertical;'
      })
    } else {
      this.setData({
        style: 'min-height:100px;border-bottom:1px solid rgb(204,204,204);'
      })
    }

    var markers = [{
      latitude: 29.94971589603503,
      longitude: 122.3691946318789,
      name: "蔬力堡普陀山店",
      desc: " 蔬力堡愿用美食来诉说爱，做一份至纯的食品，一份至善的事业！让人从身体到心灵都能变得健康轻松，用植物性饮食点亮世界。越蔬悦健康！"
    }]
    var covers = [{
      latitude: 29.94971589603503,
      longitude: 122.3691946318789,
      iconPath: '',
      rotate: 0
    }]
    this.setData({
      latitude: 29.94971589603503,
      longitude: 122.3691946318789,
      markers: markers,
      covers: covers,
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },
  onShareAppMessage: function () {
    return {
      title: app.globalData.dpxinxi.sname,
      path: '/pages/index/index',
      success: function (res) {
        // 分享成功
      }
    }
  },
  chakan: function () {
    this.setData({
      style: 'min-height:100px;',
      status: 1
    })
  },
  shouqi: function () {
    this.setData({
      style: 'display:-webkit-box;overflow:hidden;text-overflow:ellipsis;-webkit-line-clamp:4;-webkit-box-orient:vertical;',
      status: 0
    })
  }
})