// pages/yuding/index.js
var app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
      windowHeight:0,
      num:0,
      style:[
            {'style': ''},
            {'style': '' },
            {'style': '' },
            { 'style': '' },
            {'style': '' },
            { 'style': '' },
            {'style': '' },
            { 'style': '' },
            { 'style': '' },
            { 'style': '' },
            {'style': '' },
            { 'style': '' }   
      ],
      date:'请选择',
      time:'请选择',
      nowdate:'',
      focus:false,
      hidden:true,
      value:null,
      utype:null,
      dpxinxi: '',
      url:''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function () {
      var that = this
      wx.getSystemInfo({
          success: function (res) {
              that.setData({
                  windowHeight: res.windowHeight,
                  dpxinxi: app.globalData.dpxinxi,
                  url: app.globalData.severUrl
              })
          }
      })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },
  onShareAppMessage: function () {
      return {
          title: app.globalData.dpxinxi.sname,
          path: '/pages/index/index',
          success: function (res) {
              // 分享成功
          }
      }
  },
  check:function(e){
        var param1 = {}
        for (var i in this.data.style) {
            var string2 = "style[" + i + "].style"
            param1[string2] = ''
            this.setData(param1)
        }
        var j = e.currentTarget.dataset.num - 1
        var param = {}
        var string1 = "style[" + j + "].style"
        param[string1] = 'background-color:rgba(255,255,255,0.5);color:rgba(0,0,0,0.5);'
        this.setData(param)
        this.setData({
            num: e.currentTarget.dataset.num,
            hidden: true,
            focus: false,
            value: null
        })
  },
  chose:function(e){
      var param1 = {}
      for (var i in this.data.style) {
          var string2 = "style[" + i + "].style"
          param1[string2] = ''
          this.setData(param1)
      }
      var j = e.currentTarget.dataset.num - 1
      var param = {}
      var string1 = "style[" + j + "].style"
      param[string1] = 'background-color:rgba(255,255,255,0.5);color:rgba(0,0,0,0.5);'
      this.setData(param)
      this.setData({
            hidden:false,
            focus: true
      })
  },
  bindDateChange: function(e) {
    this.setData({
      date: e.detail.value
    })
  },
  bindTimeChange: function (e) {
      this.setData({
          time: e.detail.value
      })
  },

  formSubmit: function (e) {
      if (!e.detail.value.num){
          wx.showToast({
              title: '请输入人数',
              icon: 'loading',
              duration: 2000,
              mask:true
          })
          return false
      }
      if (e.detail.value.num == 0) {
          wx.showToast({
              title: '人数必须大于0',
              icon: 'loading',
              duration: 2000,
              mask: true
          })
          return false
      }
      this.setData({
          hidden: true,
          num: e.detail.value.num,
          focus: false,
          value: null
      })
  },
  navToyuding:function(e){
      if(this.data.num == 0){
          wx.showToast({
              title: '请选择人数',
              icon: 'loading',
              duration: 2000,
              mask: true
          })
          return false
      }
      wx.navigateTo({
          url: '../yuyue/index?num=' + e.currentTarget.dataset.num
      })
  }, 
  navOrder:function(e){
      app.globalData.num = e.currentTarget.dataset.num
      wx.redirectTo({
          url: '/pages/order.index?num=' 
      })
  }
})