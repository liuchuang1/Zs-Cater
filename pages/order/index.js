//index.js
//获取应用实例
var app = getApp()
Page({
  data: {
    windowHeight: 0,
    menuHeight: 0,
    yheight: 0,
    list: [],
    lists: [],
    totalnum: 0,
    totalprice: 0,
    xqHeight: 0,
    ssheight: 0,
    hidden: true,
    hidden1: '',
    pointfood: [],
    i: 0,
    u: 0,
    stype: 1,
    url: '',
    dpxinxi: '',
    bol: 0,
    typea: [],
    menu_right: [],
    menu_rights: [],
    lista: [],
    array: [],
    chooseCurrentProductId: '',
    chooseCurrentProductNumber: 0,
    imp_typeId: '',
    beijing_url: '/pages/images/beijing.jpg',
    titleimg: '/pages/images/title.jpg'
  },
  _loading: function () {
    var that = this
    wx.request({
      url: app.globalData._new_url + 'Common/GetTypeByCode', data: { shopId: app.globalData.shopids, typeCode: '1,16' },
      method: 'post',
      success: function (res) {
        var _json_type_list = JSON.parse(res.data.value)
        console.log("左侧菜单", _json_type_list)
        that.setData({
          typea: _json_type_list,
          imp_typeId: _json_type_list[0].Id
        })
      }
    })
  },
  onLoad: function (e) {
    var that = this
    wx.getSystemInfo({
      success: function (res) {
        that.setData({
          windowHeight: res.windowHeight,
          menuHeight: res.windowHeight - 112,
          xqHeight: res.windowWidth * 0.9,
          ssheight: res.windowHeight - 60
        })
      }
    })

    that._loading()
    for (var i = 0; i < this.data.list.length; i++) {
      var current = this.data.list[i];
      if (this.data.chooseCurrentProductId == current.Id) {
        this.data.chooseCurrentProductNumber = current.number
        this.setData({
          chooseCurrentProductNumber: this.data.chooseCurrentProductNumber
        })
        break
      }
    }
    this.setData({
      list: app.globalData.list,
      totalprice: app.globalData.totalprice,
      hidden: true,
      hidden1: '',
    })
  },

  changetype: function (e) {
    console.log("选中", e.currentTarget.dataset.typeid)
    var that = this
    var typeId = e.currentTarget.dataset.typeid
    var u = e.currentTarget.dataset.id
    that.setData({
      bol: u,
      imp_typeId: typeId
    })

  },
  addfoods: function (e) {
    var productId = e.currentTarget.dataset.sid
    this.setData({
      chooseCurrentProductId: productId
    })
    for (var i = 0; i < this.data.list.length; i++) {
      var current = this.data.list[i];
      if (this.data.chooseCurrentProductId == current.Id) {
        this.data.list[i].number++
        this.data.chooseCurrentProductNumber = this.data.list[i].number;
        this.setData({
          list: this.data.list,
          chooseCurrentProductNumber: this.data.chooseCurrentProductNumber
        })
        break;
      }
    }
    var totalprice = 0
    var totalnum = 0
    totalnum = this.data.totalnum + 1
    totalprice = parseFloat(this.data.totalprice) + parseFloat(e.currentTarget.dataset.price)
    totalprice = app.returnFloat(totalprice)
    this.setData({
      totalprice: totalprice,
      totalnum: totalnum
    })
    console.log("app.globalData.totalprice", app.globalData.totalprice)
    app.globalData.totalprice = this.data.totalprice
    app.globalData.list = this.data.list
  },
  checkProductIsExist: function (Array, obj) {
    var isExist = false;
    for (var i = 0; i < Array.length; i++) {
      var current = Array[i];
      if (current.sid == obj.sid) {
        current.num++;
        isExist = true;
        break;
      }
    }
    if (!isExist) {
      Array.push(obj);
    }
    return Array;
  },
  jianfoods: function (e) {
    var productId = e.currentTarget.dataset.sid
    this.setData({
      chooseCurrentProductId: productId
    })
    if (this.data.chooseCurrentProductId != '') {
      if (this.data.list.length > 0) {
        for (var i = 0; i < this.data.list.length; i++) {
          var current = this.data.list[i];
          if (this.data.chooseCurrentProductId == current.Id) {
            this.data.list[i].number--
            this.data.chooseCurrentProductNumber = this.data.list[i].number;
            this.setData({
              list: this.data.list,
              chooseCurrentProductNumber: this.data.chooseCurrentProductNumber
            })

            break;
          }
        }
        // console.log("chooseCurrentProductNumber", this.data.chooseCurrentProductNumber)
      }

      var totalprice = 0
      var totalnum = 0
      totalnum = this.data.totalnum - 1
      totalprice = parseFloat(this.data.totalprice) - parseFloat(e.currentTarget.dataset.price)
      totalprice = app.returnFloat(totalprice)
      this.setData({
        totalprice: totalprice,
        totalnum: totalnum
      })
      app.globalData.totalprice = this.data.totalprice
      app.globalData.list = this.data.list

    }

  },
  tanchu: function (e) {

    var productId = e.currentTarget.dataset.sid

    var id = e.currentTarget.dataset.id
    var ssimg = e.currentTarget.dataset.ssimg
    var ssname = e.currentTarget.dataset.ssname
    var ssdescription = e.currentTarget.dataset.description
    var ssprice = e.currentTarget.dataset.ssprice
    this.setData({
      hidden: '',
      ssimg: ssimg,
      ssname: ssname,
      // lista: this.data.dztjcpa[id],
      ssprice: ssprice,
      ssdescription: ssdescription,
      chooseCurrentProductId: productId,
    })
    for (var i = 0; i < this.data.list.length; i++) {
      var current = this.data.list[i];
      if (this.data.chooseCurrentProductId == current.Id) {
        this.data.chooseCurrentProductNumber = current.number
        this.setData({
          chooseCurrentProductNumber: this.data.chooseCurrentProductNumber
        })
        break
      }
    }
  },
  shouqi: function () {
    this.setData({
      hidden: true,
      hidden1: ''
    })
  },
  addfoods2: function (e) {
    for (var i = 0; i < this.data.list.length; i++) {
      var current = this.data.list[i];
      if (this.data.chooseCurrentProductId == current.Id) {
        this.data.chooseCurrentProductNumber = current.number
        this.setData({
          chooseCurrentProductNumber: this.data.chooseCurrentProductNumber
        })
        break
      }
    }

    if (this.data.chooseCurrentProductId != '') {
      if (this.data.list.length > 0) {
        for (var i = 0; i < this.data.list.length; i++) {
          var current = this.data.list[i];
          if (this.data.chooseCurrentProductId == current.Id) {
            this.data.list[i].number++
            this.data.chooseCurrentProductNumber = this.data.list[i].number;
            this.setData({
              list: this.data.list,
              chooseCurrentProductNumber: this.data.chooseCurrentProductNumber
            })

            break;
          }
        }
        console.log("chooseCurrentProductNumber", this.data.chooseCurrentProductNumber)
      }

      var totalprice = 0
      var totalnum = 0
      totalnum = this.data.totalnum + 1
      totalprice = parseFloat(this.data.totalprice) + parseFloat(e.currentTarget.dataset.price)
      totalprice = app.returnFloat(totalprice)
      this.setData({
        totalprice: totalprice,
        totalnum: totalnum
      })
      app.globalData.totalprice = this.data.totalprice
      app.globalData.list = this.data.list

    }
  },
  jianfoods2: function (e) {
    for (var i = 0; i < this.data.list.length; i++) {
      var current = this.data.list[i];
      if (this.data.chooseCurrentProductId == current.Id) {
        this.data.chooseCurrentProductNumber = current.number
        this.setData({
          chooseCurrentProductNumber: this.data.chooseCurrentProductNumber
        })
        break
      }
    }

    if (this.data.chooseCurrentProductId != '') {
      if (this.data.list.length > 0) {
        for (var i = 0; i < this.data.list.length; i++) {
          var current = this.data.list[i];
          if (this.data.chooseCurrentProductId == current.Id) {
            this.data.list[i].number--
            this.data.chooseCurrentProductNumber = this.data.list[i].number;
            this.setData({
              list: this.data.list,
              chooseCurrentProductNumber: this.data.chooseCurrentProductNumber
            })

            break;
          }
        }
        // console.log("chooseCurrentProductNumber", this.data.chooseCurrentProductNumber)
      }

      var totalprice = 0
      var totalnum = 0
      totalnum = this.data.totalnum - 1
      totalprice = parseFloat(this.data.totalprice) - parseFloat(e.currentTarget.dataset.price)
      totalprice = app.returnFloat(totalprice)
      this.setData({
        totalprice: totalprice,
        totalnum: totalnum
      })
      app.globalData.totalprice = this.data.totalprice
      app.globalData.list = this.data.list

    }
  },

  refreshCart: function (Array) {
    if (Array.length > 0) {
      var count = 0;
      for (var i = 0; i < Array.length; i++) {
        count += Array[i].num;
      }
      //显示层 + 把数量赋给显示数量
    }
    else {
      //隐藏层 + 层中数量改为0；
    }
  },
  checkok: function () {
    wx.switchTab({
      url: '../shopcar/index'
    })
  },
  onShow: function () {
    var that = this
    var totalnum = 0
    if (app.globalData.list == null) {
      this.setData({
        list: app.globalData.slist,
        totalprice: app.globalData.totalprice,
        totalnum: 0
      })
    } else {
      for (var i = 0; i < app.globalData.list.length; i++) {
        if (app.globalData.list[i].number > 0) {
          totalnum += app.globalData.list[i].number;
        }
      }
      that.setData({
        totalprice: app.globalData.totalprice,
        totalnum: totalnum,
        list: app.globalData.list,
        hidden: true,
        hidden1: "",
      })
    }

    console.log("购物车数量:", that.data.totalnum)//0
    console.log("缓存:", app.globalData.totalprice)//0
    console.log("当前列表", that.data.list)//[...]
    console.log("缓存列表", app.globalData.list)//null

  },
  onShareAppMessage: function () {
    return {
      title: app.globalData.dpxinxi.sname,
      path: '/pages/index/index',
      success: function (res) {
        // 分享成功
      }
    }
  },
  // hujiao: function () {
  //   wx.showModal({
  //     title: '提示',
  //     content: '确定呼叫服务员?',
  //     success: function (res) {
  //       if (res.confirm) {
  //         if (app.globalData.table == 0) {
  //           wx.showToast({
  //             title: '请先扫码确定桌号',
  //             icon: 'loading',
  //             duration: 1000,
  //             mask: true
  //           })
  //           return false
  //         } else {
  //           wx.request({
  //             url: app.globalData.severUrl + 'xiaochengxu/index/hujiaofwy',
  //             data: {
  //               shopid: app.globalData.shopid,
  //               tableid: app.globalData.table
  //             },
  //             method: 'GET',
  //             success: function (res) {
  //               console.log(res)
  //             }
  //           })
  //         }
  //       } else if (res.cancel) {
  //         console.log('用户点击取消')
  //       }
  //     }
  //   })
  // }
})
