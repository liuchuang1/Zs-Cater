// pages/paidui/index.js
var app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
        list:null
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
        var that = this
        wx.request({
            url: app.globalData.severUrl + 'xiaochengxu/index/getyuding',
            data: {
                shopid: app.globalData.shopid
            },
            method: 'GET',
            success: function (res) {
                    console.log(res)
                    that.setData({
                            list:res.data
                    })
            }
        })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },
  onShareAppMessage: function () {
      return {
          title: app.globalData.dpxinxi.sname,
          path: '/pages/index/index',
          success: function (res) {
              // 分享成功
          }
      }
  },
  linghao:function(e){
      var that = this
      var utype = e.currentTarget.dataset.type
      wx.request({
          url: app.globalData.severUrl + 'xiaochengxu/index/linghao',
          data: {
              shopid: app.globalData.shopid,
              openid: app.globalData.openId,
              utype: utype
          },
          method: 'GET',
          success: function (res) {
              if(res.data.res == 'nook'){
                  wx.showToast({
                      title: '您已经在队列中',
                      icon: 'success',
                      duration: 1000,
                      mask: true
                  })
                  setTimeout(function () {
                      wx.navigateTo({
                          url: './status?id=' + res.data.id,
                      })
                  }, 1000)
              }else{
                  wx.showToast({
                      title: '领号成功',
                      icon: 'success',
                      duration: 1000,
                      mask: true
                  })
                  setTimeout(function () {
                      wx.navigateTo({
                          url: './status?id=' + res.data.id,
                      })
                  }, 1000)
              }
          }
      })
  }
})