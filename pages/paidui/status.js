// pages/paidui/status.js
var app = getApp()
var id2 = 0
Page({

  /**
   * 页面的初始数据
   */
  data: {
        data:null
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
      wx.showToast({
          title: '加载中...',
          icon: 'loading',
          duration: 10000,
          mask: true
      })
      if (options){
          var id = options.id
          id2 = options.id
      }else{
          var id = id2
      }
      
      var that = this
      wx.request({
          url: app.globalData.severUrl + 'xiaochengxu/index/linghaores',
          data: {
                id:id
          },
          method: 'GET',
          success: function (res) {
                console.log(res)
                that.setData({
                    data:res.data
                })
                wx.hideToast()
          }
      })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },
  onShareAppMessage: function () {
      return {
          title: app.globalData.dpxinxi.sname,
          path: '/pages/index/index',
          success: function (res) {
              // 分享成功
          }
      }
  },
  ref:function(){
      this.onLoad()
  },
  ghzl:function(e){
      console.log(e.currentTarget.dataset.id)
      var that = this
      wx.request({
          url: app.globalData.severUrl + 'xiaochengxu/index/guohaozailing',
          data: {
              id: e.currentTarget.dataset.id
          },
          method: 'GET',
          success: function (res) {
              wx.navigateBack({
                  delta: 1
              })
          }
      })
  }
})