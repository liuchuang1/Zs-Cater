var app = getApp()
Page({

    /**
     * 页面的初始数据
     */
    data: {
        list:null,
        dpxinxi:null,
        url:'',
        windowHeight:0,
        hihihi:true
    },

    onLoad:function(){
        var that = this
        wx.getSystemInfo({
            success: function (res) {
                that.setData({
                    windowHeight: res.windowHeight
                })
            }
        })
        wx.request({
            url: app.globalData.severUrl + 'xiaochengxu/index/yuyuelist',
            data: {
                storeid: app.globalData.shopid,
                openid: app.globalData.openId
            },
            header: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            method: "post",
            success: function (res) {
                console.log(res)
                if(res.data == null){
                    that.setData({
                        hihihi:false
                    })
                }
                that.setData({
                    list: res.data,
                    dpxinxi: app.globalData.dpxinxi,
                    url: app.globalData.severUrl
                })
            }
        })
    },
    navtoxx:function(e){
        wx.navigateTo({
            url: '/pages/yuyue/status?id=' + e.currentTarget.dataset.id
        })
    },
    onShareAppMessage: function () {
        return {
            title: app.globalData.dpxinxi.sname,
            path: '/pages/index/index',
            success: function (res) {
                // 分享成功
            }
        }
    }

})