//index.js
//获取应用实例
var app = getApp()
Page({
  data: {
    motto: 'Hello World',
    userInfo: {},
    phone: "",
    hidden1:false,
    hidden2:true,
    texts:''
  },
  //事件处理函数
  bindViewTap: function() {
    wx.navigateTo({
      url: '../logs/logs'
    })
  },
  onLoad: function () {
    console.log('onLoad')
    var that = this
    //调用应用实例的方法获取全局数据
    app.getUserInfo(function(userInfo){
      //更新数据
      that.setData({
        userInfo:userInfo
      })
    })
  },
  yzm: function (e) {
      this.setData({
          phone: e.detail.value
      })
  },
  formSubmit:function(e){
      var phone = this.data.phone
      if (phone != e.detail.value.phone) {
          wx.showToast({
              title: '输入的手机号与验证号码不一致!',
              icon: 'loading',
              duration: 2000
          })
          return false;
      } else if (e.detail.value.phone == '') {
          wx.showToast({
              title: '手机号码不能为空!',
              icon: 'loading',
              duration: 2000
          })
          return false;
      } else if (!(/^1[34578]\d{9}$/.test(e.detail.value.phone))) {
          wx.showToast({
              title: '手机格式不正确!',
              icon: 'loading',
              duration: 2000
          })
          return false;
      } else if (e.detail.value.yzm == '') {
          wx.showToast({
              title: '验证码不能为空!',
              icon: 'loading',
              duration: 2000
          })
          return false;
      }
      var that = this
      wx.request({
          url: app.globalData.severUrl + 'xiaochengxu/index/bindPhone',
          data: {
              phone: phone,
              yzm: e.detail.value.yzm,
              uid: app.globalData.openId
          },
          method: 'GET',
          success: function (res) {
              console.log(res)
              if (res.data == '绑定成功！') {
                  wx.showToast({
                      title: '绑定成功!',
                      icon: 'success',
                      duration: 2000
                  })
                  wx.navigateTo({
                      url: '../hyzxm/hyzxm',
                  })

              } else {
                  wx.showToast({
                      title: res.data,
                      icon: 'loading',
                      duration: 2000,
                      mask:true
                  })
                  return false;
              }
          }
      })
  },
  getYzm: function () {
      var phone = this.data.phone
      if (phone == '') {
          wx.showToast({
              title: '手机号码不能为空!',
              icon: 'loading',
              duration: 2000
          })
          return false;
      } else if (!(/^1[34578]\d{9}$/.test(phone))) {
          wx.showToast({
              title: '手机格式不正确!',
              icon: 'loading',
              duration: 2000
          })
          return false;
      }
      this.setData({
          hidden1:true,
          hidden2:false,
          texts:60
      })
      this.countdown(this.data.texts)
      wx.request({
          url: app.globalData.severUrl + 'xiaochengxu/index/yzm',
          data: {
              phone: phone,
              uid: app.globalData.openId
          },
          method: 'GET',
          success: function (res) {
              console.log(res)
          }
      })
  },
  countdown: function (ob) {
      var that = this
      var second = ob
      if (second == 0) {
          console.log("Time Out...");
          that.setData({
              texts: "",
              hidden2: true,
              hidden1: false,
          });
          return;
      }
      var time = setTimeout(function () {
          that.setData({
              texts: second - 1
          });
          that.countdown(that.data.texts);
      }
          , 1000)
  },
  onShareAppMessage: function () {
      return {
          title: app.globalData.dpxinxi.sname,
          path: '/pages/index/index',
          success: function (res) {
              // 分享成功
          }
      }
  }
})
