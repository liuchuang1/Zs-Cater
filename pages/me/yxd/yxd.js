// yxd.js
var app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    style1: 'color:rgb(254,162,3);',
    style2: 'color:rgb(53,53,53);',
    style3: 'color:rgb(53,53,53);',
    list: null,
    hidden: true
  },

  /**
   * 生命周期函数--监听页面加载
   */
  _loading: function () {
    var that = this
    wx.request({
      url: app.globalData._new_url + 'WeChatLogin/GetOrderList',
      data: { openId: app.globalData.openId, shopId: app.globalData.shopids },
      method: 'post',
      success: function (res) {
        var _json_order = JSON.parse(res.data.value)
        that.setData({
          list: _json_order
        })
        console.log(_json_order) 
        wx.request({
          url: app.globalData._new_url + 'WeChatLogin/GetOrderDetail',
          data: { orderId: _json_order[0].Id },
          method: 'post',
          success: function (res) {
            console.log(res)
            var _json_orders = JSON.parse(res.data.value)
            console.log(_json_orders)
          }
        })
      }
    })
   

  },
  onLoad: function (options) {
    this._loading()
    if (options.type == 0) {
      this.setData({
        style1: 'color:rgb(254,162,3);',
        style2: 'color:rgb(53,53,53);',
        style3: 'color:rgb(53,53,53);',
      })
    } else if (options.type == 1) {
      this.setData({
        style1: 'color:rgb(53,53,53);',
        style2: 'color:rgb(254,162,3);',
        style3: 'color:rgb(53,53,53);',
      })
    } else if (options.type == 2) {
      this.setData({
        style1: 'color:rgb(53,53,53);',
        style2: 'color:rgb(53,53,53);',
        style3: 'color:rgb(254,162,3);',
      })
    }
    var that = this
    var stype = options.type
   
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },
  onShareAppMessage: function () {
    return {
      title: app.globalData.dpxinxi.sname,
      path: '/pages/index/index',
      success: function (res) {
        // 分享成功
      }
    }
  },
  change: function (e) {
    if (e.currentTarget.dataset.type == 0) {
      this.setData({
        style1: 'color:rgb(254,162,3);',
        style2: 'color:rgb(53,53,53);',
        style3: 'color:rgb(53,53,53);',
      })
    } else if (e.currentTarget.dataset.type == 1) {
      this.setData({
        style1: 'color:rgb(53,53,53);',
        style2: 'color:rgb(254,162,3);',
        style3: 'color:rgb(53,53,53);',
      })
    } else if (e.currentTarget.dataset.type == 2) {
      this.setData({
        style1: 'color:rgb(53,53,53);',
        style2: 'color:rgb(53,53,53);',
        style3: 'color:rgb(254,162,3);',
      })
    }
    var that = this
    var utype = e.currentTarget.dataset.type
    wx.request({
      url: app.globalData.severUrl + 'xiaochengxu/index/myorders',
      data: {
        utype: utype,
        openid: app.globalData.openId,
        shop_id: app.globalData.shopid
      },
      method: 'GET',
      success: function (res) {
        if (res.data == null) {
          that.setData({
            hidden: false
          })
        } else {
          that.setData({
            hidden: true
          })
        }
        that.setData({
          list: res.data
        })
      }
    })
  },
  more: function (e) {
    var i = e.currentTarget.dataset.id
    
  
  },
  shou: function (e) {
    var i = e.currentTarget.dataset.id
    var lis = this.data.list[i].cai2
    for (var j = 0; j < lis.length; j++) {
      if (j < 3) {
        lis[j].hidden = false
      } else {
        lis[j].hidden = true
      }
    }
    var param = {}
    var string1 = "list[" + i + "].cai2"
    var string2 = "list[" + i + "].more"
    param[string1] = lis
    param[string2] = 0
    this.setData(param)
  }
})