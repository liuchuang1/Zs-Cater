// grzxm.js
var app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
        url:'',
        dpxinxi:null,
        severUrl:'',
        beijing_url: '/pages/images/title.jpg'
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
        this.setData({
            dpxinxi: app.globalData.dpxinxi,
            severUrl: app.globalData.severUrl
        })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
        var that = this
        wx.request({
            url: app.globalData.severUrl + 'xiaochengxu/index/checkphone',
            data: {
                openid: app.globalData.openId
            },
            method: 'GET',
            success: function (res) {
                if (res.data == 0) {
                    that.setData({
                        url: '../hyzx/hyzx'
                    })
                } else {
                    that.setData({
                        url: '../hyzxm/hyzxm'
                    })
                }
            }
        })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },
  onShareAppMessage: function () {
      return {
          title: app.globalData.dpxinxi.sname,
          path: '/pages/index/index',
          success: function (res) {
              // 分享成功
          }
      }
  },
  call: function () {
      wx.makePhoneCall({
          phoneNumber: this.data.dpxinxi.contact //仅为示例，并非真实的电话号码
      })
  },
  call2: function () {
      wx.makePhoneCall({
          phoneNumber: '4000245898' //仅为示例，并非真实的电话号码
      })
  }
})