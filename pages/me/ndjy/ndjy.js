// ndjy.js
var app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
  
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },
  onShareAppMessage: function () {
      return {
          title: app.globalData.dpxinxi.sname,
          path: '/pages/index/index',
          success: function (res) {
              // 分享成功
          }
      }
  },
  formSubmit:function(e){
      if (e.detail.value.jianyi == ''){
          wx.showToast({
              title: '请填写内容',
              icon: 'loading',
              duration: 1000,
              mask:true
          })
          return false;
      }

      if (e.detail.value.phone == '') {
          wx.showToast({
              title: '请填写联系方式',
              icon: 'loading',
              duration: 1000,
              mask: true
          })
          return false;
      }
      var that = this
      var jianyi = e.detail.value.jianyi
      var phone = e.detail.value.phone
      // wx.request({
      //     url: app.globalData.severUrl + 'xiaochengxu/index/jianyi',
      //     data: {
      //         shopid: app.globalData.shopid,
      //         jianyi:jianyi,
      //         phone:phone
      //     },
      //     header: {
      //         'Content-Type': 'application/x-www-form-urlencoded'
      //     },
      //     method: "post",
      //     success: function (res) {
      //         if(res.data){
      //             wx.showToast({
      //                 title: '提交成功!',
      //                 icon: 'success',
      //                 duration: 1000,
      //                 mask:true
      //             })
      //             setTimeout(function () {
      //                 wx.navigateBack({
      //                     delta: 1
      //                 })
      //             }, 1000)
      //         }
      //     }
      // })
      wx.showToast({
        title: '提交成功!',
        icon: 'success',
        duration: 1000,
        mask: true
      })
      setTimeout(function () {
        wx.navigateBack({
          delta: 1
        })
      }, 1000)
  }
})