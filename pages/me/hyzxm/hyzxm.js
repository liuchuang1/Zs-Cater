// pages/hyzx/hyzx.js
var app = getApp()
Page({

    /**
     * 页面的初始数据
     */
    data: {
            userinfos:null,
            lv:0,
            hyk:0
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
            this.setData({
                userinfos: app.globalData.userInfos
            })
            var that = this
            wx.request({
                url: app.globalData.severUrl + 'xiaochengxu/index/hycard',
                data: {
                    shopid: app.globalData.shopid
                },
                method: 'GET',
                success: function (res) {
                    that.setData({
                        hyk:res.data
                    })
                }
            })
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {
        var that = this
        wx.request({
            url: app.globalData.severUrl + 'xiaochengxu/index/checkphone',
            data: {
                openid: app.globalData.openId
            },
            method: 'GET',
            success: function (res) {
                that.setData({
                    lv:res.data
                })
            }
        })
    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },
    onShareAppMessage: function () {
        return {
            title: app.globalData.dpxinxi.sname,
            path: '/pages/index/index',
            success: function (res) {
                // 分享成功
            }
        }
    },
    navTokt: function () {

    }
})