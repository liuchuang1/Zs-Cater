// pages/pay/index.js
var app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
      orderNo:'',
      tableNo:'',
      totalprice:0.00,
      level:0,
      youhui:0,
      id:0,
      utype:0,
      dpxinxi:null,
      miyao:''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
        this.setData({
            orderNo: options.orderNo,
            tableNo: options.tableNo,
            totalprice: options.totalprice,
            id: options.id,
            utype: options.type,
            dpxinxi: app.globalData.dpxinxi,
            miyao:''
        })
        var that = this
        wx.request({
            url: app.globalData.severUrl + 'xiaochengxu/index/ishuiyuan',
            data: {
                openid: app.globalData.openId,
                shopid: app.globalData.shopid
            },
            header: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            method: "post",
            success: function (res) {
                console.log(res)
                that.setData({
                    level: res.data.data,
                    miyao:res.data.miyao
                })
                if (res.data.data == 1) {
                    that.setData({
                        totalprice: app.returnFloat(that.data.totalprice * res.data.data2.discount),
                        youhui: app.returnFloat(that.data.totalprice * (1 - res.data.data2.discount))
                    })
                }
            }
        })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
     
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },
  onShareAppMessage: function () {
      return {
          title: app.globalData.dpxinxi.sname,
          path: '/pages/index/index',
          success: function (res) {
              // 分享成功
          }
      }
  },
  moneypay:function(e){
      var that = this
      wx.showModal({
          title: '提示',
          content: '现金支付申请已提交',
          showCancel:false,
          success: function (res) {
              if (res.confirm) {
                  console.log('用户点击确定')
                  wx.request({
                      url: app.globalData.severUrl + 'xiaochengxu/index/xianjinzhifu',
                      data: {
                          id: that.data.id
                      },
                      header: {
                          'Content-Type': 'application/x-www-form-urlencoded'
                      },
                      method: "post",
                      success: function (res) {
                          
                          if (res.data) {
                              wx.navigateBack({
                                  delta: 1
                              })
                          }

                      }
                  })
              } else if (res.cancel) {
                  console.log('用户点击取消')
              }
          }
      })
  },
  pay: function (e) {
      var money = e.currentTarget.dataset.money
      var miyao = this.data.miyao
      var that = this
      wx.request({
          url: app.globalData.severUrl + 'Wxpay/example/jsapi.php',
          data: {
              openId: app.globalData.openId,
              money: money,
              storeid: app.globalData.shopid
          },
          success: function (response) {
              console.log(response);
              // 发起支付(生成签名)
              var appId = response.data.appid;
              var timeStamp = (Date.parse(new Date()) / 1000).toString();
              var pkg = 'prepay_id=' + response.data.prepay_id;
              var payid = response.data.prepay_id;
              var nonceStr = response.data.nonce_str;
              var md5 = require('../../utils/md5.js');
              var paySign = md5.hex_md5('appId=' + appId + '&nonceStr=' + nonceStr + '&package=' + pkg + '&signType=MD5&timeStamp=' + timeStamp + "&key="+miyao).toUpperCase();
              //支付操作
              wx.requestPayment({
                  'timeStamp': timeStamp,
                  'nonceStr': nonceStr,
                  'package': pkg,
                  'signType': 'MD5',
                  'paySign': paySign,
                  'success': function (res) {
                      console.log(res)
                      if (res.errMsg == "requestPayment:ok"){
                          wx.request({
                                    url: app.globalData.severUrl + 'xiaochengxu/index/changeorderstatus',
                                    data: {
                                        id: that.data.id,
                                        payid: payid,
                                        openid: app.globalData.openId,
                                        money: money,
                                        storeid: app.globalData.shopid
                                    },
                                    header: {
                                        'Content-Type': 'application/x-www-form-urlencoded'
                                    },
                                    method: "post",
                                    success: function (res) {

                                        wx.request({
                                            url: app.globalData.severUrl + 'xiaochengxu/index/getToken',
                                            data: {
                                                grant_type: 'client_credential',
                                                appid: app.globalData.appid,
                                                secret: app.globalData.secret,
                                            },
                                            method: 'GET',
                                            success: function (res) {
                                                var token = res.data.access_token
                                                wx.request({
                                                    url: app.globalData.severUrl + 'xiaochengxu/index/sendMessage3',
                                                    data: {
                                                        openid: app.globalData.openId,
                                                        token: token,
                                                        payid: payid,
                                                        id:that.data.id,
                                                        storeid: app.globalData.shopid
                                                    },
                                                    method: 'POST',
                                                    header: {
                                                        'Content-Type': 'application/x-www-form-urlencoded'
                                                    },
                                                    success: function (res) {
                                                        console.log(res)
                                                    }
                                                })
                                            }
                                        })

                                        if(res.data){
                                            wx.navigateBack({
                                                delta: 1
                                            })
                                        }
                                        
                                    }
                            })
                      }
                  },
                  'fail': function (res) {
                      console.log(res)
                  }
              });
          },
          header: {
              'content-type': 'application/x-www-form-urlencoded'
          },
      });
  },
})