// pages/yuyue/index.js
var app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
      time: '12:00',
      index1:0,
      index2:0,
      num:0,
      data:null,
      date: '请选择',
      //time: '请选择',
      nowdate: '',
      nowtime:''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
      this.setData({
          num: options.num
      })
      //var date = this.gettime()
      //this.setData({
          //nowdate: date.date,
         // nowtime: date.time.slice(0, 5)
     // })
      
  },
  bindDateChange: function (e) {
      this.setData({
          date: e.detail.value
      })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
      var that = this
      wx.request({
          url: app.globalData.severUrl + 'xiaochengxu/index/yudinginfo',
          data: {
              shopid: app.globalData.shopid
          },
          method: 'GET',
          success: function (res) {
              console.log(res)
              that.setData({
                  data: res.data,
                  nowtime: res.data.ydstarttime,
                  date: res.data.ksrq
              })
          }
      })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },
  gettime: function getNowFormatDate() {
      var date = new Date();
      var seperator1 = "-";
      var seperator2 = ":";
      var month = date.getMonth() + 1;
      var strDate = date.getDate();
      if (month >= 1 && month <= 9) {
          month = "0" + month;
      }
      if (strDate >= 0 && strDate <= 9) {
          strDate = "0" + strDate;
      }
      var currentdate = []
      currentdate['date'] = date.getFullYear() + seperator1 + month + seperator1 + strDate
      currentdate['time'] = date.getHours() + seperator2 + date.getMinutes() + seperator2 + date.getSeconds();
      return currentdate;
  },
  onShareAppMessage: function () {
      return {
          title: app.globalData.dpxinxi.sname,
          path: '/pages/index/index',
          success: function (res) {
              // 分享成功
          }
      }
  },
  bindTimeChange: function (e) {
      this.setData({
          nowtime: e.detail.value
      })
  },
  bindPickerChange: function (e) {
      this.setData({
          index2: e.detail.value
      })
  },
  radioChange:function(e){
      this.setData({
          index1: e.detail.value,
          index2:0
      })
  },
  formSubmit:function(e){
      console.log(e.detail.value)
      if (e.detail.value.phone == '') {
          wx.showToast({
              title: '手机号码不能为空!',
              icon: 'loading',
              duration: 1000,
              mask: true
          })
          return false;
      } else if (!(/^1[34578]\d{9}$/.test(e.detail.value.phone))) {
          wx.showToast({
              title: '手机格式不正确!',
              icon: 'loading',
              duration: 1000,
              mask:true
          })
          return false;
      } else if (e.detail.value.date == '请选择'){
          wx.showToast({
              title: '请选择日期',
              icon: 'loading',
              duration: 1000,
              mask: true
          })
          return false;
      } else if (!e.detail.value.type2) {
          wx.showToast({
              title: '请选择餐位类型',
              icon: 'loading',
              duration: 1000,
              mask: true
          })
          return false;
      }
      var formId = e.detail.formId
      var phone = e.detail.value.phone
      var date = e.detail.value.date
      var time = e.detail.value.time
      var tableid = this.data.data.table[this.data.index1][this.data.index2].id
      var shopid = app.globalData.shopid
      var openid = app.globalData.openId
      var nickname = app.globalData.userInfos.nickName
      var headimgurl = app.globalData.userInfos.avatarUrl
      var num = this.data.num
      wx.request({
          url: app.globalData.severUrl + 'xiaochengxu/index/yudingpost',
          data: {
                phone : phone,
                date : date,
                time : time,
                tableid : tableid,
                shopid : shopid,
                openid : openid,
                nickname : nickname,
                headimgurl: headimgurl,
                num : num
          },
          header: {
              'Content-Type': 'application/x-www-form-urlencoded'
          },
          method: "post",
          success: function (res) {
              console.log(res)
              if(res.data == 'no'){
                  wx.showModal({
                      title: '提示',
                      content: '您已经预约过了,请进入"我的"-"我的预约"中查看',
                      showCancel:false,
                      success: function (res) {
                          if (res.confirm) {
                              wx.switchTab({
                                  url: '/pages/index/index'
                              })
                          }
                      }
                  })
              } else if (res.data == 'nono'){
                  wx.showModal({
                      title: '提示',
                      content: '该桌号已被预订',
                      showCancel: false,
                      success: function (res) {
                          if (res.confirm) {
                                return false
                          }
                      }
                  })
              } else if (res.data == 'nonono') {
                  wx.showModal({
                      title: '提示',
                      content: '该日期不营业',
                      showCancel: false,
                      success: function (res) {
                          if (res.confirm) {
                                return false
                          }
                      }
                  })
              } else{
                  var id = res.data
                  if (res.data) {
                      wx.showToast({
                          title: '预约成功',
                          icon: 'success',
                          duration: 1000,
                          mask: true
                      })
                      //app.globalData.table = tableid
                      wx.request({
                          url: app.globalData.severUrl + 'xiaochengxu/index/getToken',
                          data: {
                              grant_type: 'client_credential',
                              appid: app.globalData.appid,
                              secret: app.globalData.secret,
                          },
                          method: 'GET',
                          success: function (res) {
                              var token = res.data.access_token
                              wx.request({
                                  url: app.globalData.severUrl + 'xiaochengxu/index/sendMessage2',
                                  data: {
                                      openid: app.globalData.openId,
                                      token: token,
                                      formId: formId,
                                      ydid: id
                                  },
                                  method: 'POST',
                                  header: {
                                      'Content-Type': 'application/x-www-form-urlencoded'
                                  },
                                  success: function (res) {
                                      console.log(res)
                                  }
                              })
                          }
                      })
                      setTimeout(function () {
                          wx.navigateTo({
                              url: './status?id=' + res.data,
                          })
                      }, 1000)
                  }
              }
              
          }
      })
  }
})