// pages/yuyue/status.js
var app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
        data:null,
        dpxinxi: null
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
      this.setData({
          dpxinxi: app.globalData.dpxinxi
      })
      app.globalData.ordertype = 3
      app.globalData.ydid = options.id
      var id = options.id
      var that = this
      wx.request({
          url: app.globalData.severUrl + 'xiaochengxu/index/getyudinginfo',
          data: {
              id: id,
          },
          method: 'GET',
          success: function (res) {
              console.log(res)
              that.setData({
                  data: res.data
              })
          }
      })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },
  onShareAppMessage: function () {
      return {
          title: app.globalData.dpxinxi.sname,
          path: '/pages/index/index',
          success: function (res) {
              // 分享成功
          }
      }
  },
  openmap: function () {
      var that = this
      var lat = parseFloat(that.data.dpxinxi.lat)
      var lon = parseFloat(that.data.dpxinxi.lot)
      wx.getLocation({
          type: 'gcj02', //返回可以用于wx.openLocation的经纬度

          success: function (res) {
              wx.openLocation({
                  latitude: lat,
                  longitude: lon,
                  scale: 28,
                  name: that.data.dpxinxi.sname,
                  address: that.data.dpxinxi.saddress
              })
          }
      })
  },
  navtoorder:function(e){
      app.globalData.foods = null
      app.globalData.totalprice = 0.00
      app.globalData.table = this.data.data.tableid
      app.globalData.num = e.currentTarget.dataset.num
      wx.switchTab({
          url: '/pages/order/index'
      })
  },
  quxiaoyuyue:function(e){
      var that = this
      var id = e.currentTarget.dataset.id
      wx.showModal({
          title: '提示',
          content: '是否确定取消预约?',
          success: function (res) {
              if (res.confirm) {
                  wx.request({
                      url: app.globalData.severUrl + 'xiaochengxu/index/quxiaoyuyue',
                      data: {
                          id: id,
                      },
                      method: 'GET',
                      success: function (res) {
                          console.log(res)
                          if(res.data == 'no'){
                              wx.showModal({
                                  title: '提示',
                                  content: '商家已接单,请拨打商家电话协商',
                                  success: function (res) {
                                      if (res.confirm) {

                                      }
                                  }
                              })
                          }else{
                              wx.showToast({
                                  title: '已取消预约',
                                  icon: 'success',
                                  duration: 2000,
                                  mask: true
                              })
                              app.globalData.table = 0
                              setTimeout(function () {
                                  wx.switchTab({
                                      url: '/pages/index/index'
                                  })
                              }, 2000)
                          }
                      }
                  })
              } else if (res.cancel) {
                  console.log('用户点击取消')
              }
          }
      })
  },
  navtodpjs:function(){
      wx.navigateTo({
          url: '/pages/jieshao/index',
      })
  },
  quxiao:function(){
      app.globalData.table = 0
      wx.showModal({
          title: '提示',
          content: '可进入 "我的" - "我的预约" 中查看',
          success: function (res) {
              if (res.confirm) {
                  wx.switchTab({
                      url: '/pages/index/index'
                  })
              }
          }
      })
  },
  navtoorderStatus:function(e){
      wx.navigateTo({
          url: '/pages/shopcar/orderStatus?id=' + e.currentTarget.dataset.id
      })
  }
})