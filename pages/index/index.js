//index.js
//获取应用实例
var app = getApp()
Page({
  data: {
    lists: [],
    dpxinxi: null,
    url: '',
    indicatorDots: true,
    autoplay: true,
    interval: 5000,
    duration: 1000,
    dztjcp: null,
    zpccp: null,
    hidden: true,
    pointfood: [],
    i: 0,
    u: 0,
    stype: 1,
    list: [],
    totalprice: 0,
    totalnum: 0,
    id: 0,
    stype: '',
    hiddennn: false,
    bannerimgs: ["../images/banner1.jpg", "../images/banner2.jpg", "../images/banner3.jpg"],
    beijing_url: '../images/beijing.jpg',
    titleimg: '/pages/images/title.jpg'
  },
  _loading: function () {
    var that = this
    wx.request({
      url: app.globalData._new_url + 'Product/GetProAndSetMealList',
      data: { shopId: app.globalData.shopids },
      method: 'post',
      success: function (res) {
        console.log("getlist", res)
        var _json_tj = JSON.parse(res.data.value)
        console.log("菜品信息：", _json_tj)

        var list1 = new Array()
        var list2 = new Array()
        var k = 0;
        var k2 = 0;
        for (var i = 0; i < _json_tj.length; i++) {
          if (_json_tj[i].IsHot == true) {
            if (k < 4) {
              list1.unshift(_json_tj[i])
              k++;
            }
          }
          if (_json_tj[i].IsRecommand == true) {
            if (k2 < 4) {
              list2.unshift(_json_tj[i])
              k2++;
            }
          }
        }
        console.log("店长推荐", list1)
        app.globalData.list = _json_tj,
          app.globalData.slist = _json_tj,
          that.setData({
            list: _json_tj,
            dztjcpa: _json_tj,
            dztj_list: list1,
            zpc_list: list2
          })
      }
    })
  },
  onLoad: function (options) {
    var that = this
    if (options.deskid!=undefined) {
      var deskid = decodeURI(options.deskid)
      app.globalData.deskid = deskid
    }else{
      app.globalData.deskid = ""
    }
    // 店长推荐
    that._loading()
    wx.getSystemInfo({
      success: function (res) {
        that.setData({
          windowHeight: res.windowHeight + 430,
          xqHeight: res.windowWidth * 0.9,
          url: app.globalData.severUrl
        })
      }
    })
    // cs登陆
    wx.login({
      success: function (res) {
        wx.setStorageSync("code", res.code)
        wx.getUserInfo({
          success: function (res) {
            app.globalData.userInfo = res.userInfo

            if (res.code != '') {
              //发起网络请求
              var code = wx.getStorageSync("code")
              wx.request({
                url: app.globalData._new_url + 'WeChatLogin/GetSession',
                data: {
                  code: code,
                  nickName: app.globalData.userInfo.nickName,
                  appid: app.globalData.appid,
                  secret: app.globalData.secret,
                },
                method: 'post',
                success: function (res) {
                  app.globalData.openId = res.data.openid
                }
              })
            } else {
              console.log('获取用户登录态失败！')
              wx.showModal({
                title: '温馨提示',
                content: '获取用户登录态失败！',
                showCancel: false
              })
            }
          }
        })
      }

    })
    // if (app.globalData.openId == null) {
    //   if (e.id) {
    //     app.globalData.table = e.id
    //   }
    //   var shopid = app.globalData.shopid
    //   wx.login({
    //     success: function (res) {
    //       wx.showToast({
    //         title: '加载中...',
    //         icon: 'loading',
    //         duration: 10000,
    //         mask: true
    //       })
    //       if (res.code) {
    //         //发起网络请求
    //         wx.request({
    //           url: app.globalData.severUrl + 'xiaochengxu/Index/wxlogin',
    //           data: {
    //             //access_token: 'be0a8f61912712caac686d21d4f56782',
    //             js_code: res.code,
    //             appid: app.globalData.appid,
    //             secret: app.globalData.secret,
    //             grant_type: 'authorization_code'
    //           },
    //           success: function (res) {
    //             var username = res.data.openid
    //             console.log(res.data.openid)
    //             app.globalData.openId = res.data.openid
    //             wx.getUserInfo({
    //               success: function (res) {
    //                 app.globalData.userInfos = res.userInfo
    //                 var wxname = res.userInfo.nickName
    //                 var headimgurl = res.userInfo.avatarUrl
    //                 wx.request({
    //                   url: app.globalData.severUrl + 'xiaochengxu/index/check',
    //                   data: {
    //                     username: username
    //                   },
    //                   header: {
    //                     'Content-Type': 'application/x-www-form-urlencoded'
    //                   },
    //                   method: "post",
    //                   success: function (res) {
    //                     console.log(res)
    //                     if (res.data == 'no') {
    //                       wx.request({
    //                         url: app.globalData.severUrl + 'api/account',
    //                         data: {
    //                           access_token: 'daf2dc794edf40892a9453076f5a5fdb',
    //                           reg_type: 'username',
    //                           username: username,
    //                           nickname: username,
    //                           reg_verify: '',
    //                           password: '123456',
    //                           role: 1,
    //                           method: 'POST',
    //                           shopid: shopid,
    //                           wxname: wxname,
    //                           headimgurl: headimgurl
    //                         },
    //                         header: {
    //                           'Content-Type': 'application/x-www-form-urlencoded'
    //                         },
    //                         method: "post",
    //                         success: function (res) {
    //                           console.log(res)
    //                           if (res.data.info == "注册成功，请登录") {
    //                             wx.request({
    //                               url: app.globalData.severUrl + 'xiaochengxu/index/getUserinfo',
    //                               data: {
    //                                 nickname: username
    //                               },
    //                               method: 'GET',
    //                               success: function (res) {
    //                                 app.globalData.userInfo = res.data
    //                                 wx.hideToast()
    //                               }
    //                             })
    //                           }
    //                         }
    //                       })
    //                     } else {
    //                       wx.request({
    //                         url: app.globalData.severUrl + 'xiaochengxu/index/getUserinfo',
    //                         data: {
    //                           nickname: username
    //                         },
    //                         method: 'GET',
    //                         success: function (res) {
    //                           app.globalData.userInfo = res.data
    //                           wx.hideToast()
    //                         }
    //                       })
    //                     }
    //                   }
    //                 })
    //               }
    //             })
    //           }
    //         })
    //       } else {
    //         console.log('获取用户登录态失败！' + res.errMsg)
    //       }
    //     }
    //   });

    wx.request({
      url: app.globalData.severUrl + 'xiaochengxu/index/getFoodslist',
      data: {
        shopid: app.globalData.shopid
      },
      method: 'GET',
      success: function (res) {
        // wx.setNavigationBarTitle({
        //   title: "点餐系统"
        // })
        app.globalData.dpxinxi = res.data.dpxinxi
        that.setData({
          dpxinxi: res.data.dpxinxi,
        })
      }
    })

  },
  navtoJieshao: function () {
    wx.navigateTo({
      url: '../jieshao/index',
    })
  },
  navtoRemen: function (e) {
    wx.navigateTo({
      url: '/pages/foods/remen?'
    })
  }, navtoRemen2: function (e) {
    wx.switchTab({
      url: '/pages/order/index',
    })
  },
  navtowifi: function () {
    // wx.showModal({
    //   title: '提示',
    //   content: '请扫码确认桌号',
    //   showCancel: true,
    //   success: function (res) {
    //     if (res.confirm) {
    //       wx.scanCode({
    //         onlyFromCamera: true,
    //         success: (res) => {
    //           console.log(res)
    //           console.log(res.path.length) 
    //           var str = res.path
    //           var deskid = str.substring(25)
    //           console.log("desk",deskid)
    //         }
    //       })
    //     }
    //   }
    // })
    this.setData({
      hiddennn: true
    })
  },
  shouqilaia: function () {
    this.setData({
      hiddennn: false
    })
  },
  openMap: function () {
    var that = this
    console.log(app.globalData.dpxinxi)
    // var lat = parseFloat(that.data.dpxinxi.lat)
    // var lon = parseFloat(that.data.dpxinxi.lot)
    wx.getLocation({
      type: 'gcj02',
      success: function (res) {
        console.log(res)
        wx.openLocation({
          latitude: 29.94971589603503,
          longitude: 122.3691946318789,
          scale: 18,
          name: "蔬力堡普陀山店",
          address: "舟山市普陀区朱家尖码头慈航广场五号楼",
          complete: function (e) {
            console.log(e)
          }
        })
      }
    })
  },
  navtoyuyue: function (e) {
    wx.navigateTo({
      url: '/pages/yuyue/index'
    })
  },
  navtopaidui: function () {
    wx.navigateTo({
      url: '/pages/paidui/index',
    })
  },
  call: function () {
    wx.makePhoneCall({
      phoneNumber: '0580-6036336' //仅为示例，并非真实的电话号码
    })
  },
  tanchu: function (e) {
    console.log(app.globalData.list)
    var productId = e.currentTarget.dataset.sid

    var id = e.currentTarget.dataset.id
    var ssimg = e.currentTarget.dataset.ssimg
    var ssname = e.currentTarget.dataset.ssname
    var ssdescription = e.currentTarget.dataset.description
    var ssprice = e.currentTarget.dataset.ssprice
    this.setData({
      hidden: '',
      ssimg: ssimg,
      ssname: ssname,
      lista: this.data.dztjcpa[id],
      ssprice: ssprice,
      ssdescription: ssdescription,
      chooseCurrentProductId: productId,
    })
    for (var i = 0; i < this.data.list.length; i++) {
      var current = this.data.list[i];
      if (this.data.chooseCurrentProductId == current.Id) {
        this.data.chooseCurrentProductNumber = current.number
        this.setData({
          chooseCurrentProductNumber: this.data.chooseCurrentProductNumber
        })
        break
      }
    }
    console.log("chooseCurrentProductId", this.data.chooseCurrentProductId)
    console.log("chooseCurrentProductNumber", this.data.chooseCurrentProductNumber)
  },
  shouqi: function () {
    this.setData({
      hidden: true
    })
  },
  addfoods2: function (e) {
    for (var i = 0; i < this.data.list.length; i++) {
      var current = this.data.list[i];
      if (this.data.chooseCurrentProductId == current.Id) {
        this.data.chooseCurrentProductNumber = current.number
        this.setData({
          chooseCurrentProductNumber: this.data.chooseCurrentProductNumber
        })
        break
      }
    }

    if (this.data.chooseCurrentProductId != '') {
      if (this.data.list.length > 0) {
        for (var i = 0; i < this.data.list.length; i++) {
          var current = this.data.list[i];
          if (this.data.chooseCurrentProductId == current.Id) {
            this.data.list[i].number++
            this.data.chooseCurrentProductNumber = this.data.list[i].number;
            this.setData({
              list: this.data.list,
              chooseCurrentProductNumber: this.data.chooseCurrentProductNumber
            })

            break;
          }
        }
        // console.log("chooseCurrentProductNumber", this.data.chooseCurrentProductNumber)
      }

      var totalprice = 0
      var totalnum = 0
      totalnum = this.data.totalnum + 1
      totalprice = parseFloat(this.data.totalprice) + parseFloat(e.currentTarget.dataset.price)
      console.log(totalprice)
      totalprice = app.returnFloat(totalprice)
      this.setData({
        totalprice: totalprice,
        totalnum: totalnum
      })
      app.globalData.totalprice = this.data.totalprice
      app.globalData.list = this.data.list

    }


  },
  jianfoods2: function (e) {
    for (var i = 0; i < this.data.list.length; i++) {
      var current = this.data.list[i];
      if (this.data.chooseCurrentProductId == current.Id) {
        this.data.chooseCurrentProductNumber = current.number
        this.setData({
          chooseCurrentProductNumber: this.data.chooseCurrentProductNumber
        })
        break
      }
    }

    if (this.data.chooseCurrentProductId != '') {
      if (this.data.list.length > 0) {
        for (var i = 0; i < this.data.list.length; i++) {
          var current = this.data.list[i];
          if (this.data.chooseCurrentProductId == current.Id) {
            this.data.list[i].number--
            this.data.chooseCurrentProductNumber = this.data.list[i].number;
            this.setData({
              list: this.data.list,
              chooseCurrentProductNumber: this.data.chooseCurrentProductNumber
            })

            break;
          }
        }
        console.log("chooseCurrentProductNumber", this.data.chooseCurrentProductNumber)
      }

      var totalprice = 0
      var totalnum = 0
      totalnum = this.data.totalnum - 1
      totalprice = parseFloat(this.data.totalprice) - parseFloat(e.currentTarget.dataset.price)
      console.log(totalprice)
      totalprice = app.returnFloat(totalprice)
      this.setData({
        totalprice: totalprice,
        totalnum: totalnum
      })
      app.globalData.totalprice = this.data.totalprice
      app.globalData.list = this.data.list

    }
  },
  onShow: function () {
    if (this.data.list != '') {
      // console.log("onshow", this.data.list)
      // console.log(this.data.dztjcpa)
      this.setData({
        list: app.globalData.list,
        totalprice: app.globalData.totalprice
      })
    }
    wx.setNavigationBarTitle({
      title: '蔬力堡普陀山店'
    })
  },
  onHide: function () {
    this.setData({
      hidden: true
    })
  },
  onShareAppMessage: function () {

  }
})
