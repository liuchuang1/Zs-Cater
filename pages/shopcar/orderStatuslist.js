// pages/shopcar/orderStatuslist.js
var app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
      windowHeight: 0,
      kwidth: 0,
      list:null,
      dpxinxi: '',
      url:'',
      beijing_url: '/pages/images/beijing.jpg'
  },
  _loading: function () {
    var that = this
    wx.request({
      url: app.globalData._new_url + 'WeChatLogin/GetOrderList',
      data: { openId: app.globalData.openId, shopId: app.globalData.shopids },
      method: 'post',
      success: function (res) {
        var _json_order = JSON.parse(res.data.value)
        that.setData({
          list: _json_order
        })
        console.log(_json_order)
        // wx.request({
        //   url: app.globalData._new_url + 'WeChatLogin/GetOrderDetail',
        //   data: { orderId: _json_order[0].Id },
        //   method: 'post',
        //   success: function (res) {
        //     console.log(res)
        //     var _json_orders = JSON.parse(res.data.value)
        //     console.log(_json_orders)
        //   }
        // })
      }
    })


  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
      var that = this
      that._loading()
      wx.getSystemInfo({
          success: function (res) {
              that.setData({
                  windowHeight: res.windowHeight,
                  kwidth: res.windowHeight * 0.9,
                  dpxinxi: app.globalData.dpxinxi,
                  url: app.globalData.severUrl
              })
          }
      })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
      var that = this
      // wx.request({
      //     url: app.globalData.severUrl + 'xiaochengxu/index/orderStatuslist',
      //     data: {
      //         shopid: app.globalData.shopid,
      //         openid: app.globalData.openId
      //     },
      //     header: {
      //         'Content-Type': 'application/x-www-form-urlencoded'
      //     },
      //     method: "post",
      //     success: function (res) {
      //         console.log(res)
      //         that.setData({
      //             list:res.data
      //         })
      //     }
      // })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },
  onShareAppMessage: function () {
      return {
          title: app.globalData.dpxinxi.sname,
          path: '/pages/index/index',
          success: function (res) {
              // 分享成功
          }
      }
  },
  navtoxx:function(e){
    var id = e.currentTarget.dataset.id
    var deskid = e.currentTarget.dataset.deskid
    var money = e.currentTarget.dataset.money
    var time = e.currentTarget.dataset.time
    var parmid = e.currentTarget.dataset.parmid
      wx.navigateTo({
        url: './orderStatus?id=' + id + "&money=" + money + "&deskid=" + deskid + "&time=" + time + "&parmid=" + parmid
      })
  }
})