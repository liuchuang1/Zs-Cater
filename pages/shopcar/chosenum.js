// pages/yuding/index.js
var app = getApp()
Page({

    /**
     * 页面的初始数据
     */
    data: {
        windowHeight: 0,
        num: 0,
        style: [
            { 'style': '' },
            { 'style': '' },
            { 'style': '' },
            { 'style': '' },
            { 'style': '' },
            { 'style': '' },
            { 'style': '' },
            { 'style': '' },
            { 'style': '' },
            { 'style': '' },
            { 'style': '' },
            { 'style': '' }
        ],
        date: '请选择',
        time: '请选择',
        nowdate: '',
        focus: false,
        hidden: true,
        value: null,
        utype: null,
        dpxinxi: '',
        url: '',
        jixu:false,
        mlgb:''
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function () {
        var that = this
        wx.getSystemInfo({
            success: function (res) {
                that.setData({
                    windowHeight: res.windowHeight,
                    dpxinxi: app.globalData.dpxinxi,
                    url: app.globalData.severUrl
                })
            }
        })
        wx.request({
          url: app.globalData.severUrl + 'xiaochengxu/index/jnmb',
          data: {
            openid: app.globalData.openId,
            table: app.globalData.table
          },
          method: 'POST',
          header: {
            'Content-Type': 'application/x-www-form-urlencoded'
          },
          success: function (res) {
            console.log(res)
            that.setData({
              counts:res.data
            })
          }
        })
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },
    nonono:function(){
      app.globalData.jixu = !this.data.jixu
        this.setData({
          jixu:!this.data.jixu,
          num: 0,
          hidden: true,
          focus: false,
          value: null,
        })

        if(this.data.jixu == true){
            this.setData({
              mlgb:'background-color:rgba(255,255,255,0.5);color:rgba(0,0,0,0.5);'
            })
        }else{
          this.setData({
            mlgb: ''
          })
        }
        var param1 = {}
        for (var i in this.data.style) {
          var string2 = "style[" + i + "].style"
          param1[string2] = ''
          this.setData(param1)
        }
        app.globalData.xlmy = true
    },
    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },
    onShareAppMessage: function () {
        return {
            title: app.globalData.dpxinxi.sname,
            path: '/pages/index/index',
            success: function (res) {
                // 分享成功
            }
        }
    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    },
    check: function (e) {
        var param1 = {}
        for (var i in this.data.style) {
            var string2 = "style[" + i + "].style"
            param1[string2] = ''
            this.setData(param1)
        }
        var j = e.currentTarget.dataset.num 
        var param = {}
        var string1 = "style[" + j + "].style"
        param[string1] = 'background-color:rgba(255,255,255,0.5);color:rgba(0,0,0,0.5);'
        this.setData(param)
        this.setData({
            num: e.currentTarget.dataset.num,
            hidden: true,
            focus: false,
            value: null,
            jixu:false,
            mlgb: ''
        })
        app.globalData.xlmy = true
    },
    chose: function (e) {
        var param1 = {}
        for (var i in this.data.style) {
            var string2 = "style[" + i + "].style"
            param1[string2] = ''
            this.setData(param1)
        }
        var j = e.currentTarget.dataset.num 
        var param = {}
        var string1 = "style[" + j + "].style"
        param[string1] = 'background-color:rgba(255,255,255,0.5);color:rgba(0,0,0,0.5);'
        this.setData(param)
        this.setData({
            hidden: false,
            focus: true,
            jixu: false,
            mlgb: ''
        })
    },
    bindDateChange: function (e) {
        this.setData({
            date: e.detail.value
        })
    },
    bindTimeChange: function (e) {
        this.setData({
            time: e.detail.value
        })
    },

    formSubmit: function (e) {
        if (!e.detail.value.num) {
            wx.showToast({
                title: '请输入人数',
                icon: 'loading',
                duration: 2000,
                mask: true
            })
            return false
        }
        if (e.detail.value.num == 0) {
            wx.showToast({
                title: '人数必须大于0',
                icon: 'loading',
                duration: 2000,
                mask: true
            })
            return false
        }
        this.setData({
            hidden: true,
            num: e.detail.value.num,
            focus: false,
            value: null
        })
        app.globalData.xlmy = true
    },
    navToyuding: function (e) {
        var that = this
        if (this.data.num == 0 && this.data.jixu == false) {
            wx.showToast({
                title: '请选择人数',
                icon: 'loading',
                duration: 2000,
                mask: true
            })
            return false
        }
            if (app.globalData.table == 0) {
                wx.showModal({
                    title: '提示',
                    content: '请扫码确认桌号',
                    showCancel: true,
                    success: function (res) {
                        if (res.confirm) {
                            wx.scanCode({
                                onlyFromCamera: true,
                                success: (res) => {
                                    if (res.path) {
                                        var str1 = res.path.substr(21)
                                        var id = parseInt(str1)
                                        app.globalData.table = id
                                        app.globalData.num = that.data.num
                                        wx.switchTab({
                                            url: './index'
                                        })
                                    }
                                },
                                fail: (res) => {
                                    wx.switchTab({
                                        url: './index'
                                    })
                                }
                            })
                        } else if (res.cancel) {
                            wx.switchTab({
                                url: './index'
                            })
                        }
                    }
                })
        }else{
                app.globalData.num = that.data.num
                wx.switchTab({
                    url: './index'
                })
        }

        
    },
    navOrder: function (e) {
        app.globalData.num = e.currentTarget.dataset.num
        wx.redirectTo({
            url: '/pages/order.index?num='
        })
    }
})