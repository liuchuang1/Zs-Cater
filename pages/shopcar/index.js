// pages/shopcar/index.js
var app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    windowHeight: 0,
    kwidth: 0,
    num: 0,
    totalprice: 0.00,
    totalprice2: 0.00,
    lists: null,
    hidden88: true,
    hidden99: false,
    beizhu: '',
    url: '',
    ordertype: 0,
    dpxinxi: '',
    cjjg: 0.00,
    xiju: false,
    xlmy: false,
    list: null,
    chooseCurrentProductId: '',
    chooseCurrentProductNumber: 0,
    beijing_url: '/pages/images/beijing.jpg'
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    //   console.log(app.globalData.dpxinxi)
    var that = this
    wx.getSystemInfo({
      success: function (res) {
        that.setData({
          windowHeight: res.windowHeight,
          kwidth: res.windowHeight - 140
        })
      }
    })
    that.setData({
      list: app.globalData.list,
      typelist: app.globalData.foodstype,
      url: app.globalData.severUrl,
      dpxinxi: app.globalData.dpxinxi
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.setData({
      list: app.globalData.list,
      totalprice: app.globalData.totalprice,
      hidden: true,
      hidden1: '',
    })
    var totalnum = 0
    for (var i = 0; i < this.data.list.length; i++) {
      if (this.data.list[i].number > 0) {
        totalnum += this.data.list[i].number;
        this.setData({
          totalnum: totalnum
        })
      }
    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },
  onShareAppMessage: function () {
    return {
      title: app.globalData.dpxinxi.sname,
      path: '/pages/index/index',
      success: function (res) {
        // 分享成功
      }
    }
  },
  navtoorder: function () {
    wx.switchTab({
      url: '/pages/order/index'
    })
  },
  addfoods: function (e) {
    var productId = e.currentTarget.dataset.sid
    this.setData({
      chooseCurrentProductId: productId
    })
    for (var i = 0; i < this.data.list.length; i++) {
      var current = this.data.list[i];
      if (this.data.chooseCurrentProductId == current.Id) {
        this.data.list[i].number++
        this.data.chooseCurrentProductNumber = this.data.list[i].number;
        this.setData({
          list: this.data.list,
          chooseCurrentProductNumber: this.data.chooseCurrentProductNumber
        })
        break;
      }
    }
    var totalprice = 0
    var totalnum = 0
    totalnum = this.data.totalnum + 1
    totalprice = parseFloat(this.data.totalprice) + parseFloat(e.currentTarget.dataset.price)
    totalprice = app.returnFloat(totalprice)
    this.setData({
      totalprice: totalprice,
      totalnum: totalnum
    })
    app.globalData.totalprice = this.data.totalprice
    app.globalData.list = this.data.list
  },
  jianfoods: function (e) {
    var productId = e.currentTarget.dataset.sid
    this.setData({
      chooseCurrentProductId: productId
    })
    if (this.data.chooseCurrentProductId != '') {
      if (this.data.list.length > 0) {
        for (var i = 0; i < this.data.list.length; i++) {
          var current = this.data.list[i];
          if (this.data.chooseCurrentProductId == current.Id) {
            this.data.list[i].number--
            this.data.chooseCurrentProductNumber = this.data.list[i].number;
            this.setData({
              list: this.data.list,
              chooseCurrentProductNumber: this.data.chooseCurrentProductNumber
            })
            console.log(this.data.chooseCurrentProductNumber)
            break;
          }
        }
        // console.log("chooseCurrentProductNumber", this.data.chooseCurrentProductNumber)
      }

      var totalprice = 0
      var totalnum = 0
      totalnum = this.data.totalnum - 1
      totalprice = parseFloat(this.data.totalprice) - parseFloat(e.currentTarget.dataset.price)
      totalprice = app.returnFloat(totalprice)
      this.setData({
        totalprice: totalprice,
        totalnum: totalnum
      })
      app.globalData.totalprice = this.data.totalprice
      app.globalData.list = this.data.list

    }

  },
  navToorder: function () {
    wx.switchTab({
      url: '../order/index'
    })
  },
  navToorderStatus: function () {
    wx.navigateTo({
      url: './orderStatuslist'
    })
  },
  formSubmit: function (e) {
    var list = this.data.list
    if (app.globalData.deskid == '') {
      wx.showModal({
        title: '提示',
        content: '请扫码确认桌号',
        showCancel: false,
        success: function (res) {
          if (res.confirm) {
            wx.scanCode({
              onlyFromCamera: true,
              success: (res) => {
                var str = res.path
                var deskid = str.substring(25)
                console.log(deskid)
                app.globalData.deskid = deskid
                if (deskid == '') {
                  wx.showModal({
                    title: '温馨提示',
                    content: '座位号无效！',
                    showCancel: false
                  })
                  wx.hideToast()
                } else {
                  wx.showToast({
                    title: '订单提交中...',
                    icon: 'loading',
                    duration: 10000,
                    mask: true
                  })
                  var productIds = ''
                  var productNumbers = ''
                  var productIsSetMeals = ''
                  for (var i = 0; i < list.length; i++) {
                    if (list[i].number != 0) {
                      if ('' == productIds) {
                        productIds = list[i].Id
                        productNumbers = list[i].number
                        productIsSetMeals = list[i].isSetMeal
                      }
                      else {
                        productIds += ',' + list[i].Id
                        productNumbers += ',' + list[i].number
                        productIsSetMeals += ',' + list[i].isSetMeal
                      }
                    }
                  }
                  console.log("productIsSetMeals=", productIsSetMeals)

                  var that = this
                  wx.request({
                    url: app.globalData._new_url + 'WeChatLogin/AddOrderFromWeChat',
                    data: {
                      appid: app.globalData.appid,
                      openid: app.globalData.openId,
                      deskid: app.globalData.deskid,
                      productIds: productIds,
                      productNumbers: productNumbers,
                      productIsSetMeals: productIsSetMeals
                    },
                    method: 'post',
                    success: function (res) {
                      // console.log(res) 
                      wx.hideToast()
                      wx.requestPayment({
                        'timeStamp': res.data.value.timeStamp,
                        'nonceStr': res.data.value.nonceStr,
                        'package': res.data.value.package,
                        'signType': res.data.value.signType,
                        'paySign': res.data.value.paySign,
                        success: function (res) {
                          console.log("pay suc", res)
                          wx.navigateTo({
                            url: './orderStatuslist',
                            // url: '../index/index',
                          })
                        },
                      })
                    }
                  })
                  app.globalData.list = null
                  app.globalData.totalprice = 0.00
                }
              },
              fail: (res) => {
                console.log("扫码失败", res)
                wx.showModal({
                  title: '提示',
                  content: '扫码失败！',
                  showCancel: false
                })
              }
            })
          }
        }
      })
    } else {
      wx.showToast({
        title: '订单提交中...',
        icon: 'loading',
        duration: 10000,
        mask: true
      })
      var productIds = ''
      var productNumbers = ''
      var productIsSetMeals = ''
      for (var i = 0; i < list.length; i++) {
        if (list[i].number != 0) {
          if ('' == productIds) {
            productIds = list[i].Id
            productNumbers = list[i].number
            productIsSetMeals = list[i].isSetMeal
          }
          else {
            productIds += ',' + list[i].Id
            productNumbers += ',' + list[i].number
            productIsSetMeals += ',' + list[i].isSetMeal
          }
        }
      }
      console.log("deskid",app.globalData.deskid)
      var that = this
      wx.request({
        url: app.globalData._new_url + 'WeChatLogin/AddOrderFromWeChat',
        data: {
          appid: app.globalData.appid,
          openid: app.globalData.openId,
          deskid: app.globalData.deskid,
          productIds: productIds,
          productNumbers: productNumbers,
          productIsSetMeals: productIsSetMeals,
        },
        method: 'post',
        success: function (res) {
          // console.log(res)
          wx.requestPayment({
            'timeStamp': res.data.value.timeStamp,
            'nonceStr': res.data.value.nonceStr,
            'package': res.data.value.package,
            'signType': res.data.value.signType,
            'paySign': res.data.value.paySign,
            success: function (res) {
              console.log("pay suc", res)
              wx.navigateTo({
                url: './orderStatuslist',
              })
            },
            fail: function (res) {
              wx.hideToast()
              console.log("pay err", res)
             
            }
          })
        }
      })
      app.globalData.list = null
      app.globalData.totalprice = 0.00
    }
    // app.globalData.list = null
    // app.globalData.totaprice = 0.00
  },
  input: function (e) {
    this.setData({
      beizhu: e.detail.value
    })
  },
})