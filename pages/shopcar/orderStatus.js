// pages/shopcar/orderStatus.js
var app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    windowHeight: 0,
    kwidth: 0,
    lists: null,
    list: null,
    dpxinxi: '',
    url: '',
    hidden3: true,
    nimei: false,
    cjjg: 0.00,
    Id: '',
    beijing_url: '/pages/images/beijing.jpg',
  },
  xxxx: function () {
    this.setData({
      nimei: true
    })
  },
  _loading: function () {
    var that = this
    wx.request({
      url: app.globalData._new_url + 'WeChatLogin/GetOrderList',
      data: { openId: app.globalData.openId, shopId: app.globalData.shopids },
      method: 'post',
      success: function (res) {
        var _json_order = JSON.parse(res.data.value)
        that.setData({
          // list: _json_order
        })
        // console.log(_json_order)

      }
    })


  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this
    
    // that._loading()
    console.log("options", options)
    if (options) {
      that.data.id = options.id
      that.setData({
        Id: options.id,
        deskid: options.deskid,
        money: options.money,
        time: options.time,
      })
      wx.showToast({
        title: '加载中',
        icon: 'loading',
        duration: 10000
      })
      wx.request({
        url: app.globalData._new_url + 'WeChatLogin/GetOrderDetail',
        data: { orderId: options.parmid },
        method: 'post',
        success: function (res) {
          console.log(res)
          var _json_orders = JSON.parse(res.data.value)
          console.log("listlist",_json_orders)
          that.setData({
            list: _json_orders.orderDetails
          })
          wx.hideToast()
        }
      })
    }
    var that = this
    wx.getSystemInfo({
      success: function (res) {
        that.setData({
          windowHeight: res.windowHeight,
          kwidth: res.windowHeight * 0.65,
          url: app.globalData.severUrl
        })
      }
    })

    if (app.globalData.dpxinxi == null) {
      wx.request({
        url: app.globalData.severUrl + 'xiaochengxu/index/getdpxinxi',
        data: {
          storeid: app.globalData.shopid
        },
        header: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        method: "post",
        success: function (res) {
          that.setData({
            dpxinxi: res.data
          })
        }
      })
    } else {
      that.setData({
        dpxinxi: app.globalData.dpxinxi
      })
    }

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
   
    var that = this
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },
  onShareAppMessage: function () {
    return {
      title: app.globalData.dpxinxi.sname,
      path: '/pages/index/index',
      success: function (res) {
        // 分享成功
      }
    }
  },
  navToorder: function () {
    wx.switchTab({
      url: '../order/index'
    })
  },
  refresh: function () {
    this.onLoad()
  },
  navTopay: function () {
    wx.navigateTo({
      url: '../pay/index?orderNo=' + this.data.lists.orderNo + '&tableNo=' + this.data.lists.tableNo + '&totalprice=' + this.data.lists.totalprice + '&id=' + this.data.lists.id + '&type=' + this.data.lists.type
    })
  },
  chulai: function () {
    this.setData({
      hidden3: false
    })
  },
  shouqi: function () {
    this.setData({
      hidden3: true
    })
  }
})