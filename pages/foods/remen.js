// pages/lists/remen.js
var app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    windowHeight: 0,
    headHeight: 0,
    listHeight: 0,
    top: 0,
    imgHeight: 0,
    list: [],
    lists: [],
    totalnum: 0,
    totalprice: 0,
    utype: '',
    url: '',
    hidden: true,
    imgimg: '',
    hidden4: true,
    i: 0,
    u: 0,
    id: 0,
    pointlist: null,
    chooseCurrentProductNumber:0,
    chooseCurrentProductId:'',
    remenimg:'/pages/images/remen.jpg'
  },
  tanchu: function (e) {

    var productId = e.currentTarget.dataset.sid
console.log("productid:",productId)
    var id = e.currentTarget.dataset.id
    var ssimg = e.currentTarget.dataset.ssimg
    var ssname = e.currentTarget.dataset.ssname
    var ssdescription = e.currentTarget.dataset.description
    var ssprice = e.currentTarget.dataset.ssprice
    this.setData({
      hidden4: '',
      ssimg: ssimg,
      ssname: ssname,
      // lista: this.data.dztjcpa[id],
      ssprice: ssprice,
      ssdescription: ssdescription,
      chooseCurrentProductId: productId,
    })
    for (var i = 0; i < this.data.list.length; i++) {
      var current = this.data.list[i];
      if (this.data.chooseCurrentProductId == current.Id) {
        this.data.chooseCurrentProductNumber = current.number
        this.setData({
          chooseCurrentProductNumber: this.data.chooseCurrentProductNumber
        })
        break
      }
    }
  },
  shouqi: function () {
    this.setData({
      hidden4: true
    })
  },

  onLoad: function (options) {
    
    // wx.request({
    //   url: app.globalData._new_url + 'Product/GetProAndSetMealList',
    //   data: { shopId: app.globalData.shopids },
    //   method:'post',
    //   success:function(res){
    //     var _json_ishot = JSON.parse(res.data.value)
    //     console.log(_json_ishot)
    //     that.setData({
    //       list:_json_ishot
    //     })
    //   }
    // })
    this.setData({ 
      url: app.globalData.severUrl,
      list: app.globalData.list
    })
    var that = this
    wx.getSystemInfo({
      success: function (res) {
        that.setData({
          windowHeight: res.windowHeight,
          headHeight: res.windowHeight * 0.3,
          listHeight: res.windowHeight * 0.66,
          top: res.windowHeight * 0.26,
          imgHeight: res.windowHeight * 0.63 * 0.28 * 0.7,
          xqHeight: res.windowWidth * 0.9
        })
      }
    })

  },
  addfoods: function (e) {
    var productId = e.currentTarget.dataset.sid
    this.setData({
      chooseCurrentProductId: productId
    })
    for (var i = 0; i < this.data.list.length; i++) {
      var current = this.data.list[i];
      if (this.data.chooseCurrentProductId == current.Id) {
        this.data.list[i].number++
        this.data.chooseCurrentProductNumber = this.data.list[i].number;
        this.setData({
          list: this.data.list,
          chooseCurrentProductNumber: this.data.chooseCurrentProductNumber
        })
        break;
      }
    }
    var totalprice = 0
    var totalnum = 0
    totalnum = this.data.totalnum + 1
    totalprice = parseFloat(this.data.totalprice) + parseFloat(e.currentTarget.dataset.price)
    totalprice = app.returnFloat(totalprice)
    this.setData({
      totalprice: totalprice,
      totalnum: totalnum
    })
    console.log("app.globalData.totalprice", app.globalData.totalprice)
    app.globalData.totalprice = this.data.totalprice
    app.globalData.list = this.data.list
  },
  jianfoods: function (e) {
    var productId = e.currentTarget.dataset.sid
    this.setData({
      chooseCurrentProductId: productId
    })
    if (this.data.chooseCurrentProductId != '') {
      if (this.data.list.length > 0) {
        for (var i = 0; i < this.data.list.length; i++) {
          var current = this.data.list[i];
          if (this.data.chooseCurrentProductId == current.Id) {
            this.data.list[i].number--
            this.data.chooseCurrentProductNumber = this.data.list[i].number;
            this.setData({
              list: this.data.list,
              chooseCurrentProductNumber: this.data.chooseCurrentProductNumber
            })

            break;
          }
        }
        // console.log("chooseCurrentProductNumber", this.data.chooseCurrentProductNumber)
      }

      var totalprice = 0
      var totalnum = 0
      totalnum = this.data.totalnum - 1
      totalprice = parseFloat(this.data.totalprice) - parseFloat(e.currentTarget.dataset.price)
      totalprice = app.returnFloat(totalprice)
      this.setData({
        totalprice: totalprice,
        totalnum: totalnum
      })
      app.globalData.totalprice = this.data.totalprice
      app.globalData.list = this.data.list

    }

  },
  addfoods2: function (e) {
    for (var i = 0; i < this.data.list.length; i++) {
      var current = this.data.list[i];
      if (this.data.chooseCurrentProductId == current.Id) {
        this.data.chooseCurrentProductNumber = current.number
        this.setData({
          chooseCurrentProductNumber: this.data.chooseCurrentProductNumber
        })
        break
      }
    }

    if (this.data.chooseCurrentProductId != '') {
      if (this.data.list.length > 0) {
        for (var i = 0; i < this.data.list.length; i++) {
          var current = this.data.list[i];
          if (this.data.chooseCurrentProductId == current.Id) {
            this.data.list[i].number++
            this.data.chooseCurrentProductNumber = this.data.list[i].number;
            this.setData({
              list: this.data.list,
              chooseCurrentProductNumber: this.data.chooseCurrentProductNumber
            })

            break;
          }
        }
        console.log("chooseCurrentProductNumber", this.data.chooseCurrentProductNumber)
      }

      var totalprice = 0
      var totalnum = 0
      totalnum = this.data.totalnum + 1
      totalprice = parseFloat(this.data.totalprice) + parseFloat(e.currentTarget.dataset.price)
      totalprice = app.returnFloat(totalprice)
      this.setData({
        totalprice: totalprice,
        totalnum: totalnum
      })
      app.globalData.totalprice = this.data.totalprice
      app.globalData.list = this.data.list

    }
  },
  jianfoods2: function (e) {
    for (var i = 0; i < this.data.list.length; i++) {
      var current = this.data.list[i];
      if (this.data.chooseCurrentProductId == current.Id) {
        this.data.chooseCurrentProductNumber = current.number
        this.setData({
          chooseCurrentProductNumber: this.data.chooseCurrentProductNumber
        })
        break
      }
    }

    if (this.data.chooseCurrentProductId != '') {
      if (this.data.list.length > 0) {
        for (var i = 0; i < this.data.list.length; i++) {
          var current = this.data.list[i];
          if (this.data.chooseCurrentProductId == current.Id) {
            this.data.list[i].number--
            this.data.chooseCurrentProductNumber = this.data.list[i].number;
            this.setData({
              list: this.data.list,
              chooseCurrentProductNumber: this.data.chooseCurrentProductNumber
            })

            break;
          }
        }
        // console.log("chooseCurrentProductNumber", this.data.chooseCurrentProductNumber)
      }

      var totalprice = 0
      var totalnum = 0
      totalnum = this.data.totalnum - 1
      totalprice = parseFloat(this.data.totalprice) - parseFloat(e.currentTarget.dataset.price)
      totalprice = app.returnFloat(totalprice)
      this.setData({
        totalprice: totalprice,
        totalnum: totalnum
      })
      app.globalData.totalprice = this.data.totalprice
      app.globalData.list = this.data.list

    }
  },

  compare: function (property) {
    return function (a, b) {
      var value1 = a[property];
      var value2 = b[property];
      return value1 - value2;
    }
  },
  onShow: function () {
    var that = this
    var totalnum = 0
    if (app.globalData.list == null) { 
      app.globalData.list = app.globalData.slist
      that.setData({
        list: app.globalData.list,
        totalprice: app.globalData.totalprice,
        totalnum: 0,
      })
    } else {
      for (var i = 0; i < app.globalData.list.length; i++) {
        if (app.globalData.list[i].number > 0) {
          totalnum += app.globalData.list[i].number;
        }
      }
      that.setData({
        totalprice: app.globalData.totalprice,
        totalnum: totalnum,
        list: app.globalData.list,
      })
    }

  },
  checkok: function () {
    wx.switchTab({
      url: '../shopcar/index'
    })
  },
  onShareAppMessage: function () {
    return {
      title: app.globalData.dpxinxi.sname,
      path: '/pages/index/index',
      success: function (res) {
        // 分享成功
      }
    }
  }
})