// pages/shopcar/orderStatus.js
var app = getApp()
Page({

    /**
     * 页面的初始数据
     */
    data: {
        windowHeight: 0,
        kwidth: 0,
        lists: null,
        list: null,
        dpxinxi: '',
        url: '',
        hidden3: true,
        openid:'',
        hidden: true
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {

        var id = options.id
        var openid = options.openid
        var that = this
        wx.getSystemInfo({
            success: function (res) {
                that.setData({
                    windowHeight: res.windowHeight,
                    kwidth: res.windowHeight * 0.65,
                    url: app.globalData.severUrl
                })
            }
        })

        if (app.globalData.dpxinxi == null ){
            wx.request({
                url: app.globalData.severUrl + 'xiaochengxu/index/getdpxinxi',
                data: {
                    storeid: app.globalData.shopid
                },
                header: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                method: "post",
                success: function (res) {
                    that.setData({
                        //dpxinxi: app.globalData.dpxinxi,
                        dpxinxi: res.data
                    })
                }
            })
        }else{
            that.setData({
                dpxinxi: app.globalData.dpxinxi
            })
        }

        wx.request({
            url: app.globalData.severUrl + 'xiaochengxu/index/orderStatus',
            data: {
                id: id
            },
            header: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            method: "post",
            success: function (res) {
                console.log(res)
                if (res.data) {
                    var lis = null
                    lis = JSON.parse(res.data.content)
                    that.setData({
                        lists: res.data,
                        list: lis
                    })
                } else {
                    that.setData({
                        lists: null,
                        list: null
                    })
                }
                wx.hideToast()
            }
        })

    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
  

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },
  
    formSubmit:function(e){
        wx.showToast({
            title: '加载中',
            icon: 'loading',
            duration: 10000,
            mask: true
        })
        var id = this.data.lists.id
        var formId = e.detail.formId
        var openid = this.data.openid
        wx.request({
            url: app.globalData.severUrl + 'xiaochengxu/index/shenhe',
            data: {
               id:id
            },
            method: 'GET',
            success: function (res) {
                if(res.data == 1){
                    wx.request({
                        url: app.globalData.severUrl + 'xiaochengxu/index/getToken',
                        data: {
                            grant_type: 'client_credential',
                            appid: app.globalData.appid,
                            secret: app.globalData.secret,
                        },
                        method: 'GET',
                        success: function (res) {
                            var token = res.data.access_token
                            wx.request({
                                url: app.globalData.severUrl + 'xiaochengxu/index/sendMessage5',
                                data: {
                                    //openid: app.globalData.openId,
                                    token: token,
                                    formId: formId,
                                    id:id,
                                    openid:openid
                                },
                                method: 'POST',
                                header: {
                                    'Content-Type': 'application/x-www-form-urlencoded'
                                },
                                success: function (res) {
                                    console.log(res)
                                    wx.showToast({
                                        title: '已审核',
                                        icon: 'success',
                                        duration: 1000,
                                        mask: true
                                    })
                                    setTimeout(function () {
                                        wx.switchTab({
                                            url: '/pages/index/index'
                                        })
                                    }, 1000)
                                }
                            })
                        }
                    })
                    
                }
            }
        })
    },
    formSubmit2:function(e){
        wx.showToast({
            title: '加载中',
            icon: 'loading',
            duration: 10000,
            mask: true
        })
        var formId = e.detail.formId
        var id = this.data.lists.id
        var yy = e.detail.value.yy.replace(/(^\s*)|(\s*$)/g, "")
        if(yy == '' || yy==' '){
            wx.showToast({
                title: '请填写原因',
                icon: 'loading',
                duration: 1000,
                mask: true
            }) 
            return false
        }
        wx.request({
            url: app.globalData.severUrl + 'xiaochengxu/index/getToken',
            data: {
                grant_type: 'client_credential',
                appid: app.globalData.appid,
                secret: app.globalData.secret,
            },
            method: 'GET',
            success: function (res) {
                var token = res.data.access_token
                wx.request({
                    url: app.globalData.severUrl + 'xiaochengxu/index/sendMessage6',
                    data: {
                        //openid: app.globalData.openId,
                        token: token,
                        formId: formId,
                        id: id,
                        yy:yy
                    },
                    method: 'POST',
                    header: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    success: function (res) {
                        console.log(res)
                        wx.showToast({
                            title: '已拒绝',
                            icon: 'success',
                            duration: 1000,
                            mask: true
                        })
                        setTimeout(function () {
                            wx.switchTab({
                                url: '/pages/index/index'
                            })
                        }, 1000)
                    }
                })
            }
        })
    },
    butongguo:function(){
        this.setData({
            hidden:false
        })
    },
    butongguo2: function () {
        this.setData({
            hidden: true
        })
    }
})