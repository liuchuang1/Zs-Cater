//app.js
App({
  onLaunch: function () {
    //调用API从本地缓存中获取数据
    var logs = wx.getStorageSync('logs') || []
    logs.unshift(Date.now())
    wx.setStorageSync('logs', logs)

  },
  // _loading: function () {
  //   var _self = this
  //   wx.request({
  //     url: app.globalData._new_url + 'Product/GetList',
  //     data: { shopId: '222' },
  //     method: 'post',
  //     success: function (res) {
  //       var _json_product_list = JSON.parse(res.data.value)
  //       console.log("list", _json_product_list)
  //       _self.setData({
  //         lista: _json_product_list
  //       })
  //     }
  //   })
  // },
  getUserInfo: function (cb) {
    var that = this
    if (this.globalData.userInfo) {
      typeof cb == "function" && cb(this.globalData.userInfo)
    } else {
      //调用登录接口
      wx.login({
        success: function () {
          wx.getUserInfo({
            success: function (res) {
              that.globalData.userInfo = res.userInfo
              typeof cb == "function" && cb(that.globalData.userInfo)
            }
          })
        }
      })
    }
  },
  globalData: {
    severUrl: '',
    _new_url: '',
    userInfo: null,
    userInfos: null,
    foods: null,
    foods2: null,
    foodstype: null,
    totalprice: 0.00,
    appid: '',
    secret: '',
    openId: null,
    id: 0,
    ordertype: 1,
    ydid: null,
    dpxinxi: null,
    num: 0,
    shopid: 69,
    shopids: 'a5c244fe-7295-49e4-b37b-180136bdf12e',
    table: 0,
    jixu: false,
    xlmy: false,
    chooseCurrentProductId: '',
    chooseCurrentProductNumber: 0,
    list: null,
    slist:null,
    deskid:''
  },
  returnFloat: function (value) {
    var value = Math.round(value * 100) / 100;
    var xsd = value.toString().split(".");
    if (xsd.length == 1) {
      value = value.toString() + ".00";
      return value;
    }
    if (xsd.length > 1) {
      if (xsd[1].length < 2) {
        value = value.toString() + "0";
      }
      return value;
    }
  }
})